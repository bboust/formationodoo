# -*- coding: utf-8 -*-
__author__ = 'odoo'

from openerp import models, fields, api

class Entity(models.Model):
    _name = 'epc.entity'
    _inherit = 'mail.thread'

    name = fields.Char(required=True)
    validity_start = fields.Datetime()
    validity_end = fields.Datetime()
    parent_id = fields.Many2one('epc.entity', string = 'Parent entity', select=True, ondelete='restrict') # ondelete = restrict -> Si on veut delete une entity qui a des activité, on peut aps la supprimer
    childs_ids = fields.One2many('epc.entity', 'parent_id', string = 'Child entities')
    parent_left = fields.Integer('Left Parent', select=1) #nom du champs obligatoire (pas au choix) et select doit etre mis
    parent_right = fields.Integer('Right Parent', select=1)
    activities_charge_ids = fields.One2many('epc.activityinfo', 'entity_charge_id', string='Activities')
    activities_attrib_ids = fields.One2many('epc.activityinfo', 'entity_attr_id', string='Attributed activities')

    _parent_name = 'parent_id'
    _parent_store = True # stocké en DB
    _parent_order = 'name' # tri
    _order = 'parent_left'
