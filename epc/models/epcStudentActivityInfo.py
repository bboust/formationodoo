__author__ = 'odoo'

from openerp import fields, models, api


class EpcStudentActivityInfo(models.Model):
    _name='epc.student.activityinfo'

    activityinfo_id = fields.Many2one('epc.activityinfo', string='Activity info')
    student_id = fields.Many2one('res.partner', domain="[('student', '=', 'True')]", string='Student')
    registration_date = fields.Date("Registration date")