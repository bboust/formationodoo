# -*- coding: utf-8 -*-
__author__ = 'odoo'

from openerp import models, fields, api

class Activity(models.Model):
    _name = 'epc.activity' #nom de la table (en remplacant les '.' par des '_'
    _inherit = 'mail.thread'
    _description = "Activity"
    _order = "sequence"

    name = fields.Char(track_visibility='always')
    sequence = fields.Integer() # pour sauvegarder un ordre de tri avec le widget handle dans la vue
    responsible_id = fields.Many2one('res.partner', string="Responsible")
    ens_start = fields.Integer(track_visibility='always')
    ens_end = fields.Integer(track_visibility='always')
    description = fields.Text()
    specset_ids = fields.One2many('epc.specset', "activity_id", string="cahier de charge")
    activityinfo_ids = fields.One2many('epc.activityinfo', 'activity_id', string="Infos") #Non stocké dans la DB !
    active = fields.Boolean('Active', default=True)
    country_id = fields.Many2one('res.country', related="responsible_id.country_id") # pour changer champs d'une autre entité (uniquement possible sur Many2one)
