# -*- coding: utf-8 -*-
{
    'name': "epc",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Your Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['mail', 'board', 'website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
        'views/menu.xml',
        'views/activity.xml',
        'views/cahierCharge.xml',
        'views/partner.xml',
        'views/activityinfo.xml',
        'views/entity.xml',
        'views/activityinfo_workflow.xml',
        'views/resultWizard.xml',
        'views/activityinfoanalysis.xml',
        'views/epc_board.xml',
        'security/groups.xml',
        'reports/activityinfo.xml',
        'templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}