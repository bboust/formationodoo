__author__ = 'odoo'


# -*- coding: utf-8 -*-
from openerp import fields, models, api


class Partner(models.Model):
    _inherit = 'res.partner'
    # Add a new column to the res.partner model, by default partners are not
    # instructors
    instructor = fields.Boolean("Instructor", default=False)
    student = fields.Boolean("Student", default = False)
    activityinfo_ids = fields.Many2many('epc.activityinfo',
                                        relation="epc_student_activityinfo",
                                        column1='student_id',
                                        column2='activity_id')
    courses_count = fields.Integer("Number of courses", compute = '_get_nb_courses')
    courses_daily = fields.Char('Skills', compute='_get_courses_daily')
    courses_prc = fields.Integer('Percentage of courses', compute='_get_prc_courses')
    info_ids = fields.One2many('epc.student.activityinfo', 'student_id', string="Students & activityinfos")
    # registration_date = fields.Date(fields.Date.today)

    @api.one
    def _get_courses_daily(self):
        a = unicode([
            { "tooltip" : "old", "value" : 5},
            { "tooltip" : "new", "value" : 7},
            { "tooltip" : "nzw 1", "value" : 7},
            { "tooltip" : "new 2", "value" : 6},
        ]).replace("'", "\" ")
        self.courses_daily = a

    @api.one
    def _get_nb_courses(self):
        self.courses_count = len(self.activityinfo_ids)

    @api.one
    @api.depends('activityinfo_ids')
    def _get_prc_courses(self):
        nbr = self.env['epc.activityinfo'].sudo().search_count([])
        if not nbr :
            self.courses_prc = 0.0
        else:
            self.courses_prc = 100.0 * len(self.activityinfo_ids) / nbr

    @api.multi
    def courses_list(self):
        return {
            'name' : 'Student courses',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'epc.activityinfo',
            'type' : 'ir.actions.act_window',
            'domain' : [['id', 'in', self.activityinfo_ids.ids]],
            # 'domain' : [['student_ids.id', '=', self.id]],
        }