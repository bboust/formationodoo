# -*- coding: utf-8 -*-
__author__ = 'odoo'

from openerp import models, fields, api

class SpecSet(models.Model):
    _name = 'epc.specset'
    _description="cahier de charge"
    _rec_name= 'validity'

    validity=fields.Integer('validity') #le String correspond au label du champs dans les html
    activity_id=fields.Many2one('epc.activity', string="Activity")
    language=fields.Char()
    skills=fields.Html('Skills to aquire')
    prerequisite=fields.Html('Prerequisite')

    _sql_constraints = [('activlang_uniq', 'unique(activity_id,language)', 'Specification set must be unique by language-activity!')]



    @api.multi
    def copy(self, default=None):
        default = dict(default or {})
        # import pudb
        # pu.db
        #Si on ne met pas le deuxieme domaine activity_id = ... , alors on va appliquer cette méthode sur TOUS les cahiers de charge
        copied_count = self.search_count(
            [('language', '=like', u"{}%".format(self.language)), ('activity_id','=',self.activity_id.id)])
        if not copied_count:
            new_language = u"{} (1)".format(self.language)
        else:
            new_language = u"{} ({})".format(self.language, copied_count)

        default['language'] = new_language
        return super(SpecSet, self).copy(default)
