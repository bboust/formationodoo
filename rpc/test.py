__author__ = 'odoo'
import xmlrpclib

HOST = "localhost"
PORT = 8069
DB = 'ucl'
USER = 'admin'
PASS = 'admin'

root = 'http://%s:%d/xmlrpc/' % (HOST,PORT)

# 1. Login
uid = xmlrpclib.ServerProxy(root + 'common').login(DB,USER,PASS)
print "Logged in as %s (uid:%d)" % (USER,uid)


# Create a new note
sock = xmlrpclib.ServerProxy(root + 'object')
args = {
    'name' : "Cree par RPC",
    'description' : 'Use XML RPC',
    'create_uid' : uid,
}
note_id = sock.execute(DB, uid, PASS, 'epc.activity', 'create', args)

# call = functools.partial(
#     xmlrpclib.ServerProxy(ROOT + 'object').execute,
#     DB, uid, PASS)

# 2. Read the sessions
# sessions = call('openacademy.session','search_read', [], ['name','seats'])
# for session in sessions:
#     print "Session %s (%s seats)" % (session['name'], session['seats'])
# # 3.create a new session
# session_id = call('openacademy.session', 'create', {
#     'name' : 'My session',
#     'course_id' : 2,
# })
